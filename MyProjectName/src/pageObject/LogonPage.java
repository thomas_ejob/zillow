package pageObject;

import org.openqa.selenium.By;

public class LogonPage {
	// Main page
	public static final By logonButton			= By.id("login_opener");
	public static final By registerButton		= By.id("register_opener");

	//references inside the IFRAME
	public static final By iFrameLogon			= By.xpath(".//*[@id='login_content']/iframe");
	public static final By iFrameEmail			= By.id("email");
	public static final By iFramePassword		= By.id("password");
	
	// references inside the submit
	public static final By iFrameSubmit			= By.id("loginSubmit");

	//references inside the register
	public static final By iFrameRegister		= By.id("registerSubmit");
	public static final By iFrameProfessional	= By.id("proCheckbox");		//checkbox: I am an industry professional

	public static final By iFrameProfCategory	= By.id("profession-category");
	public static final By iFrameFirstName		= By.id("firstName");
	public static final By iFrameLastName		= By.id("lastName");
	public static final By iFrameZip			= By.id("zip");
	public static final By iFramePhoneAreaCode	= By.id("areaCode");
	public static final By iFramePhonePrefix	= By.id("prefix");
	public static final By iFramePhoneNumber	= By.id("number");
	public static final By iFramePhoneExtension	= By.id("extension");
}