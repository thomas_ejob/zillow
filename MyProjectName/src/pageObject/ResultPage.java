package pageObject;

import org.openqa.selenium.By;

public class ResultPage {
	// =========================================================================================================
	// Menu selection
	// =========================================================================================================
	
	public static final By menu1_listing		= By.id("listings-menu-label");
		public static final By menu10_saleListing	= By.id("fs-listings-input");		//Sale listings
			public static final By menu100_agent		= By.id("fs-agent-input");			//agent
			public static final By menu101_owner		= By.id("fs-owner-input");			//owner
			public static final By menu102_newHomes		= By.id("fs-new-input");			//new Homes
			public static final By menu103_foreclosure	= By.id("fs-foreclosures-input");	//Foreclosure
			public static final By menu104_comingSoon	= By.id("fs-comingsoon-input");		//Coming soon
		public static final By menu11_pListing		= By.id("pm-listings-input");		//Potential listings
			public static final By menu110_foreclosure	= By.id("pm-foreclosed-input");		//Foreclosed
			public static final By menu111_preForeclosure= By.id("pm-pre-foreclosure-input");//Pre-Foreclosure
			public static final By menu112_MMM		= By.id("pm-mmm-input");			//MMM = Make Me Move
		public static final By menu12_rent			= By.id("fr-listings-input");		//For Rent
		public static final By menu13_recentlySold	= By.id("rs-listings-input");		//Recently Sold
		public static final By menu14_openHouse		= By.id("open-houses-input");		//Open Houses only
		public static final By menu15_pendingListing= By.id("pending-listings-input");	//Include Pending Listings
	public static final By menu2_price			= By.id("price-menu-label");
		public static final By menu2_price_min = By.xpath("//div[@id='price-min-options']/ul");
		public static final By menu2_price_max = By.xpath("//div[@id='price-max-options']/ul");
	public static final By menu3_beds			= By.id("beds-menu-label");		
		public static By getBed(int index)
		{
			return By.xpath("//div[@id='beds-entries']/ul[@id='bed-options']/li["+ (index+1) +"]");
		}
	public static final By menu4_homeType		= By.id("type-menu-label");
		public static final By menu40_sfr			= By.id("hometype-sf-input");			//houses
		public static final By menu41_apartment		= By.id("hometype-mf-input");			//apartement
		public static final By menu42_condo			= By.id("hometype-condo-input");		//condos/co-ops
		public static final By menu43_townhouse		= By.id("hometype-townhome-input");		//townhome
		public static final By menu44_manufactured	= By.id("hometype-manufactured-input");	//Manufactured
		public static final By menu45_lots			= By.id("hometype-land-input");			//Lots/land
	public static final By menu5_more			= By.className("menu-label");
	
	
	public static final By resultCounter		= By.id("map-result-count-message");	//To check if the page is loaded
	public static final By searchInput			= By.id("citystatezip");				//Search address

	// =========================================================================================================
	// PROPERTY CHARACTERISTIC
	// =========================================================================================================
	
	public static final By properties			= By.tagName("article");
	public static final By propertyTypeSale		= By.cssSelector("dt[class='type-forSale type show-icon']");
	public static final By propertyTypeRent		= By.cssSelector("dt[class='type-forRent type show-icon']");
	public static final By propertyTypeListing	= By.cssSelector("dt[class='type-MMM type show-icon']");
	public static final By propertyTypeRecentlySold	= By.cssSelector("dt[class='type-recentlySold type show-icon']");

	public static final By propertyAddress		= By.className("property-address");
	public static final By propertyData			= By.className("property-data");
	public static final By propertyLotsize		= By.className("property-lot");
	public static final By propertyYear			= By.className("property-year");
	public static final By propertyPrice		= By.className("price-large");
	public static final By propertyZEstimate	= By.className("zestimate");
	public static final By propertyDoz			= By.className("doz");
}