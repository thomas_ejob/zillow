package others;


abstract class A
{
	int i;				//This does NOT need to be defined. ZERO by default
	public void publicFunction()
	{
		System.out.println("CLASS A");
	}
	public abstract void test();
}	
class B extends A
{
	int i=10;
	int j=10;

	//NO CHOICE: test() had to be defined 
	public void test()
	{
		System.out.println("CLASS B");
	}
}

public class TestAbstract {
	public static void main(String[] args) {
		//A a1 = new A();		//NOT ALLOWED: Abstract cannot be ever instantiated
		//B b1 = new A();		//NOT ALLOWED: Abstract cannot be ever instantiated
		A a2 = new B();
		B b2 = new B();
		a2.publicFunction();
		b2.publicFunction();
		a2.test();
		b2.test();
		System.out.println(a2.i);				//a2.j is UNDEFINED
		System.out.println(b2.i +"__"+ b2.j);
	}
}
