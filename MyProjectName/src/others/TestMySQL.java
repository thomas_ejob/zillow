package others;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class TestMySQL {
	private static Connection connect	= null;
	private static Statement statement	= null;
	private static PreparedStatement preparedStatement = null;
	private static ResultSet resultSet	= null;

	public static void main(String[] args) throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost/zillow?user=root&password=MySQL");

			// statements allow to issue SQL queries to the database
			statement = connect.createStatement();
			// resultSet gets the result of the SQL query
			resultSet = statement.executeQuery("select count(*) from zillow.user");
			writeResultSet(resultSet);		      
		} catch (Exception e) {
			System.out.println("==>"+ e);
		} finally {
			resultSet.close();
			statement.close();
			connect.close();
		}
	}
	private static void writeResultSet(ResultSet resultSet) throws SQLException {
		// resultSet is initialised before the first data set

		while (resultSet.next()) {
			String username = resultSet.getString("username");
			String password = resultSet.getString("password");
			int industry_id = resultSet.getInt("industry_id");
			Date registration_date = resultSet.getDate("registration_date");

			System.out.println("username: "+ username);
			System.out.println("password: "+ password);
			System.out.println("industry_id: "+ industry_id);
			System.out.println("Date: " + registration_date);
		}
	}
}