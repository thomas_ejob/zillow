package others;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;



@RunWith(Parameterized.class)
public class testMkyong {
    @BeforeClass
    public static void oneTimeSetUp() {
    	System.out.println("@BeforeClass - oneTimeSetUp");
    }
    @AfterClass
    public static void oneTimeTearDown() {
    	System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Before
    public void setUp() {
        System.out.println("@Before - setUp");
    }
 
    @After
    public void tearDown() {
        System.out.println("@After - tearDown");
    }	
	
	
	@Test
	public void assertEqualsException() {
		assertEquals("10 x 0 must be 0", 2, 2);
		System.out.println("assertEqualsException END");
	}
	@Test
	public void assertTrueException() {
		assertTrue("10 x 0 must be 0", true);
		System.out.println("assertTrueException END");
	}
	@Test(expected = ArithmeticException.class)
	public void divisionWithException() {
		int i = 1/0;
		System.out.println("divisionWithException END");
	}
	@Test(timeout = 1000)
	public void infinity() {
		while (true);
	}
	@Ignore("Not Ready to Run")
	public void someMethod() {
		System.out.println("someMethod END");
	}
}