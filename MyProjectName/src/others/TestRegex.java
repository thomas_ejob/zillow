package others;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestRegex {
	public static void main(String[] args) {
		
		String input = "xx0 123 xx1 456+ XX1 1233 XXX3"; 
		String pattern = "";
		System.out.println("STRING    : "+ input);
 
		//Search a particular input
		pattern = "(?is)(.*)(text1)(.*)";
		if (input.matches(pattern))
			System.out.println("There is at least one match");
		
		/*
		//Replace the first number by @
			pattern = "(\\d)";
			System.out.println("BASIC0: "+ input.replaceFirst(pattern, "@") );
		//Replace each number by @
			pattern = "(\\d)";
			System.out.println("BASIC1: "+ input.replaceAll(pattern, "@") );
		//Insert one @ between each char, replace all series of numbers by one @
			pattern = "(\\d*)";
			System.out.println("BASIC2: "+ input.replaceAll(pattern, "@") );
		//Replace all series of numbers by one @
			pattern = "(\\d+)";
			System.out.println("BASIC3: "+ input.replaceAll(pattern, "@") );
		//Replace all numbers or "+" by one @
			pattern = "[\\d+]";
			System.out.println("BASIC4: "+ input.replaceAll(pattern, "@") );
		*/

		/*
		//Replace TWO to THREE letters in a row by @
			pattern = "[a-zA-Z]{2,3}";
			System.out.println("DUPLICATE1: "+ input.replaceAll(pattern, "@") );
		//Remove TWO DUPLICATE characters in a row
			pattern = "(\\w)\\1";		//this does the same: "(\\w+)\\1"
			System.out.println("DUPLICATE2: "+ input.replaceAll(pattern, "@") );
		//Remove ALL DUPLICATE characters in a row
			pattern = "(\\w)\\1+";		//this does the same: "(\\w+)\\1+"
			System.out.println("DUPLICATE3: "+ input.replaceAll(pattern, "@") );
		//Remove ALL DUPLICATE words 
			pattern = "(?i)\\b(\\w+)\\b([\\w\\W]*)\\b(\\1)\\b";
			System.out.println("DUPLICATE4: "+ input.replaceAll(pattern, "$1 $2") );
		*/

		/*
		//SPLIT INTO ARRAY (patterns are the delimiters)
		pattern = "(?i)\\b(xx1)\\b";
		String[] splitString = input.split(pattern);
		for (String string : splitString)
		      System.out.println("SPLIT INTO:"+ string +"#");
		*/

		/*
		//SPLIT INTO LISTARRAY (patterns are the results)
		input = "I have a cat, but I like my dog better.";
		Pattern p = Pattern.compile("(mouse|cat|dog|wolf|bear|human)");
		Matcher m = p.matcher(input);
	
		List<String> animals = new ArrayList<String>();
		System.out.print("Found: ");
		while (m.find()) {
			System.out.println("GOT:"+ m.group() +"#");
			animals.add(m.group());
		}
		*/
	}
}