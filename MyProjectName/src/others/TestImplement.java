package others;


interface C
{
	int i=5;			//This must be defined
	//Only public or abstract functions
	public void publicFunction();
	public void test();
}	
class D implements C
{
	int i=10;
	int j=10;

	//NO CHOICE: 
	@Override
	public void publicFunction() {
		System.out.println("CLASS B");
	}
	@Override
	public void test() {
		System.out.println("CLASS B");
	}
}
public class TestImplement {
	public static void main(String[] args) {
		//C c1 = new C();		//NOT ALLOWED: Abstract cannot be ever instantiated
		//D d1 = new C();		//NOT ALLOWED: Abstract cannot be ever instantiated
		C c2 = new D();
		D d2 = new D();
		c2.test();
		d2.test();
		System.out.println(c2.i);				//c2.j is UNDEFINED
		System.out.println(d2.i +"__"+ d2.j);
	}
}
