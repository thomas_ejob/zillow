package appModules;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//Based on: http://www.programcreek.com/2013/10/efficient-counter-in-java/
public class MyHashMap {
	private Map<String, String> map = new HashMap<String, String>();

	
	public MyHashMap() {
	}
	public void ViewAll() {
		Iterator<String> keySetIterator = map.keySet().iterator();

		while(keySetIterator.hasNext()){
		  String key = keySetIterator.next();
		  System.out.println("key/value: " + key + "__" + map.get(key));
		}
	}
	public int getSize() {
		return map.size();		
	}

	public boolean checkKey(String key) {
		return map.containsKey(key);
	}
	public boolean checkValue(String value) {
		return map.containsValue(value);
	}
	public boolean checkAll(String key, String value) {
		return get(key).equals(value);
	}

	public boolean add(String key) {
		map.put(key, "");
		return true;
	}
	public boolean add(String key, String value) {
		map.put(key, value);
		return true;
	}
	public String get(String key) {
		return map.get(key);
	}
	public boolean removeKey(String key) {
		map.remove(key);
		return true;
	}
}