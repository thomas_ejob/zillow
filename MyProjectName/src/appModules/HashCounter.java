package appModules;

import java.util.HashMap;
import java.util.Map;

//Based on: http://www.programcreek.com/2013/10/efficient-counter-in-java/
public class HashCounter {
	private Map<String, int[]> intCounter = new HashMap<String, int[]>();


	public HashCounter() {
	}
	public HashCounter(String key) {
		add(key);
	}


	public void add(String key) {
		int[] valueWrapper = intCounter.get(key);
		if (valueWrapper == null)
			intCounter.put(key, new int[] { 1 });
		else
			valueWrapper[0]++;
	}
	public int getCount(String key) {
		int[] valueWrapper = intCounter.get(key);
		if (valueWrapper == null)
			return 0;
		else
			return intCounter.get(key)[0];
	}
}