package appModules;


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import utility.Constant;


public class Property {
	private static final Logger logger = LogManager.getLogger("logger");
	private MySQL mySQL;
	private Integer mySQL_cityID;
	private Integer mySQL_listingID;
	private Integer mySQL_propertyID;

	private Message errorLog;

	private PropertyType type;	//House For Sale
	private String street;		//1474 Tularcitos Dr, Milpitas, CA
	private String city;		//1474 Tularcitos Dr, Milpitas, CA
	private int GLA;	
	private int lotsize;	
	private int bedroom;		//3 beds, 3 baths, 1,866 sqft
	private Double bathroom;	//3 beds, 3 baths, 1,866 sqft
	private int buildYear;		//Built in 1978

	private int price;			//$835,000
	private int zEstimate;		//ZestimateŽ: $727K
	private int doz;			//19 days on Zillow


	public Property(String type, String address, String data, String lotsize, String buildYear, String price, String zEstimate, String doz) {
		mySQL			= new MySQL();
		errorLog		= new Message("Property.class");
		
		this.type		= PropertyType.get(type);
		this.street		= getDataIndex(address, 0);
		this.city		= getDataIndex(address, 1);
		this.GLA		= Value.convertDoubleInt( getDataNb(data, "sqft") );
		this.lotsize	= getLotSize(lotsize);
		this.bedroom	= Value.convertDoubleInt( getDataNb(data, "bed") );
		this.bathroom	= getDataNb(data, "bath");
		this.buildYear	= Value.getInt(buildYear);
		this.price		= Value.getInt(price);		//Sometimes it is the rent price (ex: $4,500/mo-> 4500 )
		this.zEstimate	= getZEstimate(zEstimate);
		this.doz		= Value.getInt(doz);
		
		if ("".equals(street)) errorLog.add("street");
		if ("".equals(city)) errorLog.add("city");
		if ("".equals(GLA)) errorLog.add("GLA");
		if ("".equals(lotsize)) errorLog.add("lotsize");
		if ("".equals(bedroom)) errorLog.add("bedroom");
		if ("".equals(bathroom)) errorLog.add("bathroom");
		if ("".equals(buildYear)) errorLog.add("year");
		if ("".equals(price)) errorLog.add("price");
		if ("".equals(zEstimate)) errorLog.add("zEstimate");
		if ("".equals(doz)) errorLog.add("doz");
		String message = errorLog.toString(false);
		if (!"".equals(message))
			logger.error("Property \""+ address +"\" is missing "+ message );

		//PS: during actual test, we have access to the data in the DB and therefore we don't need the following lines)
		mySQL_cityID 	= MySQL_getCityID();
		mySQL_listingID	= MySQL_getListingID(mySQL_cityID);
		mySQL_propertyID= MySQL_getPropertyID(mySQL_cityID);
		//Populate MySQL's property table
		if ((mySQL_cityID!=null) && (MySQL_propertyNb(mySQL_cityID)==0))
		{
			String SQLquery= "INSERT INTO property (street, cityID, GLA, lotsize, bedroom, bathroom, buildYear) VALUES ('"+ this.street +"', '"+ mySQL_cityID +"', '"+ this.GLA +"', '"+ this.lotsize +"', '"+ this.bedroom +"', '"+ this.bathroom +"', '"+ this.buildYear +"');";
			mySQL.executeUpdate(SQLquery);
			mySQL_propertyID= MySQL_getPropertyID(mySQL_cityID);
		}
		if ((mySQL_cityID!=null) && (mySQL_listingID!=null) && (mySQL_propertyID!=null) && (MySQL_listingNb(mySQL_cityID, mySQL_listingID, mySQL_propertyID)==0)) 
		{
			String SQLquery= "INSERT INTO listing (propertyID, listingTypeID, price, zEstimate, doz) VALUES ("+ mySQL_propertyID +", "+ mySQL_listingID +", "+ this.price +", "+ this.zEstimate +", '"+ Value.getDateTime(this.doz) +"');";
			mySQL.executeUpdate(SQLquery);
		}
	}


	public Double getDataNb(String data, String item) {
		String pattern = "^(?is)(\\S+)(\\s+)("+ item +")(s?)$";
		String[] data_a = data.split(",(\\s)+");
		for (String aData : data_a)
		{
			if (aData.matches(pattern))
				return Value.convertStrDouble( aData.replaceAll(pattern, "$1") );
		}
		return 0.0;
	}
	//Function to get the address and the city
	public String getDataIndex(String data, int index) {
		try {
			return data.split("(\\s)*,(\\s)*")[index];
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("out of bound for "+ data +"/"+ index);
		}
	}
	private int getLotSize(String lotsize) {
		double d = Value.getDouble(lotsize);
		if (d<Constant.lotSizeMin)
			//This lotsize is in acres
			d = Value.convertAcresInt(d);
		return Value.convertDoubleInt(d);
	}
	private int getZEstimate(String zEstimate) {
		double d = Value.getDouble(zEstimate);
		if ((d>0) && (d<Constant.PriceMin))
			//This price is in millions
			d *= 1000;
		return Value.convertDoubleInt(d);
	}


	//Check the information in the database
	public Integer MySQL_getCityID() {
		String SQLquery = "SELECT cityID FROM location WHERE cityName=\""+ city +"\";";
		return mySQL.getInt(SQLquery);
	}
	public Integer MySQL_getListingID(int cityID) {
		String SQLquery = "SELECT listingTypeID FROM listingType WHERE listingName=\""+ getType() +"\";";
		return mySQL.getInt(SQLquery);
	}
	public Integer MySQL_getPropertyID(int cityID) {
		String SQLquery = "SELECT propertyID FROM property WHERE (street=\""+ street +"\") AND (cityID=\""+ cityID +"\");";
		return mySQL.getInt(SQLquery);
	}
	public Integer MySQL_propertyNb(int cityID) {
		String SQLquery = "SELECT count(*) FROM property WHERE (street='"+ street +"') AND (cityID="+ cityID +") AND (GLA="+ GLA +") AND (lotsize="+ lotsize +") AND (bedroom="+ bedroom +") AND (bathroom="+ bathroom +") AND (buildYear="+ buildYear +")";   
		return mySQL.getInt(SQLquery);
	}
	public Integer MySQL_listingNb(int cityID, int listingTypeID, int propertyID) {
		String SQLquery = "SELECT count(*) FROM LISTING WHERE (propertyID='"+ propertyID +"') AND (listingTypeID='"+ listingTypeID +"') AND (price='"+ price +"') AND (zEstimate='"+ zEstimate +"');";
		return mySQL.getInt(SQLquery);
	}
	public boolean MySQLcheck() {
		boolean result = true;
		
		int propertyNb = MySQL_propertyNb(mySQL_cityID);
		if (propertyNb==0) {
			logger.error("Property \""+ street +"\" is missing in DB");
			result = false;
		}
		else if (propertyNb>1) {
			logger.error("Property \""+ street +"\" has "+ propertyNb +" duplicates in DB");
			result = false;
		}

		int listingNb = MySQL_listingNb(mySQL_cityID, mySQL_listingID, mySQL_propertyID);
		if (listingNb==0) {
			logger.error("Listing ID \""+ mySQL_propertyID +"\" is missing in DB");
			result = false;
		}
		else if (listingNb>1) {
			logger.error("Listing ID (propertyID = \""+ mySQL_propertyID +"\") has "+ listingNb +" duplicates in DB");
			result = false;
		}
		return result;
	}

	
	//Get stuffs
	public String summary() {
		//For testing purpose
		return type +": "+ street +"___"+ price;
	}
	public String getType() {
		return type.name();
	}
	public String getStreet() {
		return street;
	}
	public String getCity() {
		return city;
	}
	public int getGLA() {
		return GLA;
	}
	public int getLotsize() {
		return lotsize;
	}
	public int getBedroom() {
		return bedroom;
	}
	public Double getBathroom() {
		return bathroom;
	}
	public int getYear() {
		return buildYear;
	}
	public int getPrice() {
		return price;
	}
	public int getZEstimate() {
		return zEstimate;
	}
	public int getDoz() {
		return doz;
	}
}