package appModules;

public class StopWatch {
    private long startTime;
    private long lapTime;
    
	public StopWatch() {
		startTime = System.nanoTime();
		lapTime = startTime; 
	}

	public void resetAll() {
		startTime = System.nanoTime();
		lapTime = startTime; 
	}
	public void resetLapTime() {
		lapTime = System.nanoTime();
	}
	public long getNanoseconds(boolean fromLapTime) {
		long result = 0;
		if (fromLapTime)
			result = System.nanoTime() - lapTime;
		else
			result = System.nanoTime() - startTime;

		resetLapTime();
	    return result;
	}
	public double getSeconds(boolean fromStartTime) {
	    double result = 0.0;
		if (fromStartTime)
			result = (double)(System.nanoTime() - startTime) / 1000000000.0;
		else
			result = (double)(System.nanoTime()-lapTime) / 1000000000.0;
	    
		resetLapTime();
	    return result;
	}
}