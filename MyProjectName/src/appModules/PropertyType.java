package appModules;

public enum PropertyType {
	SALE, RENT, LISTING, RECENTLY_SOLD;

	public static PropertyType get(String key) {
		if (key != null)
			for (PropertyType b : PropertyType.values())
				if ( key.equalsIgnoreCase(b.name()) )
					return b;
		throw new IllegalArgumentException("No enum with text " + key + " found");
	} 
}