package appModules;

import java.util.ArrayList;
import java.util.List;

public class Message {
	private List<String> message = new ArrayList<String>();
	private String className;

	
	public Message(String className) {
		this.className = className;
	}


	public void add(String newText) {
		message.add(newText);
	}
	public String toString(boolean lineByLine) {
		if (message.size()==0)
			return "";
		else if (message.size()==1)
			return className +":"+ message.get(0);
		else
		{
			if (lineByLine) {
				//Separate the message line by line
				String result = className +":";
				for (String s : message)
					result += "\n- "+ s;
				return result;
			}
			else {
				//Separate the message using comma
				return className +":"+ message.toString();
			}
		}
	}
}