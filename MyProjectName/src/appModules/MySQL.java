package appModules;

import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import utility.Constant;

public class MySQL {
	private static Connection connect	= null;
	private static Statement statement	= null;
	private static ResultSet resultSet	= null;

	public MySQL() {
		open();
	}
	public boolean open()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost/"+ Constant.MySQL_Schema +"?user="+ Constant.MySQL_login +"&password="+ Constant.MySQL_password);
			// statements allow to issue SQL queries to the database
			statement = connect.createStatement();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean close()
	{
		boolean passAll = true;
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				passAll = false;
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				passAll = false;
			}
		}
		if (connect != null) {
			try {
				connect.close();
			} catch (SQLException e) {
				passAll = false;
			}
		}
		return passAll;
	}

	/*
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		List<List<String>> tableList = new ArrayList<List<String>>();
		open();
		tableList = selectResult("select count(*) from zillow.user", false);
		System.out.print("@"+ tableList.get(0).get(0) );
		
		//tableList = selectResult("select * from zillow.user", false);
		for (List<String> root : tableList) {
			for (String s : root) {
				System.out.print("@"+ s);
			}
			System.out.println("");
		}
		close();
		System.out.println("==> DONE");
	}
	*/
	public List<List<String>> selectResult(String SQLquery, boolean fieldLabel) {
		List<List<String>> tableList = new ArrayList<List<String>>();
	    try {
			// resultSet gets the result of the SQL query
			resultSet = statement.executeQuery(SQLquery);

	        ResultSetMetaData metaData = resultSet.getMetaData();
	        int columnNb = metaData.getColumnCount();

	        //Get the labels
	        if (fieldLabel)
	        {
	            List<String> fieldList = new ArrayList<String>();
	            for (int i=1; i<=columnNb; i++)
	            {
	            	fieldList.add( metaData.getColumnLabel(i) );
	            }
	            tableList.add(fieldList);
	        }
            
            //Get the data from the table
	        while (resultSet.next()) {
	            List<String> rowList = new ArrayList<String>();
	            for (int i=1; i<=columnNb; i++)
	            {
	            	rowList.add(resultSet.getString(i));
	            }
	            tableList.add(rowList);
	        }
	    }
	    catch (Exception e) {
	    	System.out.println("selectResult="+ e);
	    	e.printStackTrace();
	    }
	    return tableList;
	}
	public Integer getInt(String SQLquery) {
		Integer i = 0;
		try
		{
			if (selectResult(SQLquery, false).size()==0)
				return null;
			i = Value.convertStrInt( selectResult(SQLquery, false).get(0).get(0) );
		}
	    catch (Exception e) {
	    	e.printStackTrace();
	    }
		return i;
	}
	public boolean executeUpdate(String SQLquery) {
	    try {
			statement.executeUpdate(SQLquery);
			statement.executeQuery("commit;");
			return true;
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return false;
	}
}