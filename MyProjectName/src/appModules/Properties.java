package appModules;

import java.util.ArrayList;
import java.util.List;

public class Properties {
	private List<Property> propertyList = new ArrayList<Property>();

	/*
	private int GLA;
	private int lotsize;
	private int price;			//$835,000
	private int zEstimate;		//ZestimateŽ: $727K
	private int doz;			//19 days on Zillow
	*/

	private HashCounter counterType;
	private HashCounter counterCity;
	private HashCounter counterBedroom;
	private HashCounter counterBathroom;
	private HashCounter counterYear;
	
	public Properties() {
		counterType		= new HashCounter();
		counterCity		= new HashCounter();
		counterBedroom	= new HashCounter();
		counterBathroom	= new HashCounter();
		counterYear		= new HashCounter();
	}
	public void add(Property property)
	{
		propertyList.add(property);

		counterType.add( property.getType() );
		counterCity.add( property.getCity() );
		counterBedroom.add( Value.convertIntStr(property.getBedroom()) );
		counterBathroom.add( Value.convertDoubleStr( property.getBathroom() ) );
		counterYear.add( Value.convertIntStr( property.getYear() ) );
	}
	public Property get(int index)
	{
		if ((index>=0) && (index < size()))
			return propertyList.get(index);
		throw new IndexOutOfBoundsException("index ="+ index);
	}
	public int size()
	{
		return propertyList.size();
	}
	public void clear()
	{
		propertyList.clear();
	}


	public int countType(PropertyType listing)
	{
		return counterType.getCount( listing.name() );
	}
	public int countCity(String city)
	{
		return counterCity.getCount( city );
	}
	public int countBedroom(int bedroomNb)
	{
		return counterBedroom.getCount( Value.convertIntStr(bedroomNb) );
	}
	public int countBathroom(Double bathroomNb)
	{
		return counterBathroom.getCount( Value.convertDoubleStr(bathroomNb) );
	}
	public int countYear(int year)
	{
		return counterYear.getCount( Value.convertIntStr(year) );
	}
}