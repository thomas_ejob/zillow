package appModules;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Value {
	public static int convertAcresInt(Double data) {
		try {
			data *= 43560;
			return data.intValue();
		} catch (Exception e) {
			//throw new NumberFormatException("fail to convert "+ data +" from Double to int");
			return 0;
		}
	}
	public static int convertDoubleInt(Double data) {
		try {
			return data.intValue();
		} catch (Exception e) {
			//throw new NumberFormatException("fail to convert "+ data +" from Double to int"); 
			return 0;
		}
	}
	public static Double convertIntDouble(int data) {
		try {
			return (double) data;
		} catch (Exception e) {
			//throw new NumberFormatException("fail to convert "+ data +" from int to Double"); 
			return 0.0;
		}
	}
	public static int convertStrInt(String data) {
		try {
			return Integer.parseInt( cleanNumber(data) );
		} catch (Exception e) {
			//throw new NumberFormatException("fail to convert "+ data +" from String to int");
			return 0;
		}
	}
	public static String convertIntStr(int data) {
		try {
			//return new Integer(data).toString();
			return Integer.toString(data);			//lighter, faster and doesn't use extra memory
		} catch (Exception e) {
			//throw new ClassCastException("fail to convert "+ data +" from int to String"); 
			return "";
		}
	}
	public static Double convertStrDouble(String data) {
		try {
			return Double.parseDouble( cleanNumber(data) );
		} catch (NumberFormatException e) {
			//throw new NumberFormatException("fail to convert "+ data +" from String to Double");
			return 0.0;
		}
	}
	public static String convertDoubleStr(Double data) {
		try {
			//return new Double(data).toString();
			return Double.toString(data);			//lighter, faster and doesn't use extra memory
		} catch (Exception e) {
			//throw new ClassCastException("fail to convert "+ data +" from Double to String"); 
			return "";
		}
	}
	
	

	public static int getInt(String data) {
		data = data.replaceAll("([,])*", "");
		data = data.replaceAll("^(^[^\\d-.]*)([-]?)([\\d]*)(.*)", "$2$3");
		return convertStrInt(data);
	}
	public static Double getDouble(String data) {
		data = data.replaceAll("([,])*", "");
		data = data.replaceAll("^(^[^\\d-.]*)([-]?)([\\d]*)([\\.]?)([\\d]*)(.*)", "$2$3$4$5");
        return convertStrDouble(data);
	}
	public static String getString(String data) {
		String result = "";
	    Scanner fi = new Scanner(data);
	    //anything other than alphanumeric characters, comma, dot or negative sign is skipped
	    fi.useDelimiter("[^\\p{Alnum},\\.-]"); 
	    while (true) {
	        if (fi.hasNextInt())
	        	fi.nextInt();
	        else if (fi.hasNextDouble())
	        	fi.nextDouble();
	        else if (fi.hasNext())
	        	result = fi.next();
	        else
	            break;
	    }
	    fi.close();
        return result;
	}
	
	private static String cleanNumber(String number)
	{
		number = number.trim();
		number = number.replaceAll(",", "");
		return number;
	}
	
	public static String getDateTime(int offDays) {
		//Removing days
		offDays = offDays*-1;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, offDays);
        return dateFormat.format( calendar.getTime() );
	}
}