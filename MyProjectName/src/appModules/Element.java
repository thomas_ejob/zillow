package appModules;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Element {
	private static final Logger logger = LogManager.getLogger("logger");
	private WebDriver driver;
	private final int TIMEOUT = 10;

	
	public Element(WebDriver driver) {
		this.driver = driver;
	}

	// ==============================================================================================
	// URL
	// ==============================================================================================

	public void goToURL(String baseUrl) {
		driver.get(baseUrl);
		//driver.manage().window().maximize();
	}
	public String getURL()
	{
		return driver.getCurrentUrl();
	}
	public boolean checkURL(String partialURLExpected) {
		return getURL().matches("(?is)(.*)("+ partialURLExpected +")(.*)");
	}

	// ==============================================================================================
	// TITLE
	// ==============================================================================================
	
	public String getTitle() {
		return driver.getTitle();
	}
	public boolean checkTitle(String partialTitleExpected) {
		return getTitle().matches("(?is)(.*)("+ partialTitleExpected +")(.*)");
	}

	// ==============================================================================================
	// wait and get the WebElement
	// ==============================================================================================

	public WebElement getVisibleElement(By by) {
		//Wait for the element to be visible (FASTER)
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		return webElement;
	}
	public WebElement getPresentElement(By by) {
		//Wait for the element to be present
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT);
		WebElement webElement = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		return webElement;
	}
	public WebElement getWebElement(WebElement webElementRoot, String tagName, String value)
	{
		//Get the getWebElement from tagnames
		List<WebElement> webElementList = webElementRoot.findElements( By.tagName(tagName) );
		for (WebElement webElement : webElementList) {
			if ( webElement.getText().equals(value) )
				return webElement;
		}
		return null;
	}

	public boolean isElementPresent(By by) {
		//Check if the element is present right now
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);  
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	// ==============================================================================================
	// General functions tested for execution speed
	// ==============================================================================================

	public String getData(WebElement webElement, By by, boolean exceptionWarning)
	{
		try {
			if (isElementPresent(by))
				return webElement.findElement(by).getText().trim();
		}
		catch(Exception e) {
			if (exceptionWarning) {
				//e.printStackTrace();
				logger.error("==> getData cannot find: "+ by.toString());
			}
		}
		return "";
	}
	public boolean checkBox(By by, boolean checked) {
		try {
			WebElement webElement = getPresentElement(by);
			if (webElement.isSelected() != checked)
				webElement.click();
			return true;
		} catch(Exception e) {
			e.printStackTrace();
		}			
		return false;
	}
	public boolean inputForm(By by, String input) {
		try
		{
			WebElement webElement = getPresentElement(by);
			webElement.click();
			webElement.clear();
			webElement.sendKeys(input.trim());
			return true;
		} catch(Exception e){
			e.printStackTrace();
		}			
		return false;
	}
	public boolean clickButton(By by) {
		try
		{
			if ( isElementPresent( by ))
				getPresentElement(by).click();	//we need a small delay 
			return true;
		} catch(Exception e){
			e.printStackTrace();
		}			
		return false;
	}
}