package main;

import static org.junit.Assert.*;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import appModules.Element;
import appModules.MyHashMap;
import appModules.MySQL;
import appModules.Properties;
import appModules.Property;
import appModules.PropertyType;
import appModules.StopWatch;
import appModules.Value;
import pageObject.LogonPage;
import pageObject.ResultPage;



@RunWith(Parameterized.class)
public class Zillow_4j2 {
	private static final Logger logger = LogManager.getLogger("logger");

	private WebDriver driver;
	private Element element;
	private Properties properties;
	private String baseUrl;

	// ================================================================================================================

	private MyHashMap parameter;

	@Parameters
	public static Collection<Object[]> data() {
		MyHashMap map1 = new MyHashMap();
		map1.add("saleListing");
		map1.add("sfr");
		map1.add("bed", "2");
		map1.add("priceMin", "$300,000+");
		map1.add("priceMax", "$700,000");

		MyHashMap map2 = new MyHashMap();
		map2.add("saleListing");
		map2.add("sfr");
		map2.add("townhouse");
		map2.add("bed", "3");
		
		Object[][] data = new Object[][] { {map1}, {map2} };
		return Arrays.asList(data);
	}

	public Zillow_4j2(MyHashMap parameter) {
		this.parameter = parameter;
	}

	// ================================================================================================================

	@Before
	public void setUp() throws Exception {
		//System.out.println("@@"+ logger +"@@"+ Zillow_4j2.class.getName() +"@@");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		element = new Element(driver);
		properties = new Properties();
		baseUrl = "http://www.zillow.com/";
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		driver	= null;
		element	= null;
		properties.clear();
	}

	@Test (timeout = 120000)
	public void saleListingTest() throws Exception {
		System.out.println("==> checkSaleListing test START");

		baseUrl = "http://www.zillow.com/homes/for_sale/Milpitas-CA-95035/97940_rid/37.544169,-121.709633,37.356923,-121.998711_rect/11_zm/1_fr/";
		StopWatch time = new StopWatch();
		element.goToURL(baseUrl);
		logger.trace("Time to load the page: "+ time.getSeconds(false));

		/*
		logon(Constant.email, Constant.password);
		element.inputForm(ResultPage.searchInput, "146 Falcato Dr, Milpitas, CA 95035");
		driver.findElement(ResultPage.searchInput).submit();
		*/
		
		//Select only sale listings
		element.clickButton(ResultPage.menu1_listing);
		element.checkBox( ResultPage.menu10_saleListing, parameter.checkKey("saleListing"));
		element.checkBox( ResultPage.menu11_pListing, parameter.checkKey("pListing"));			//= potential listings
		element.checkBox( ResultPage.menu12_rent, parameter.checkKey("rent"));					
		element.checkBox( ResultPage.menu13_recentlySold, parameter.checkKey("recentlySold"));	
		element.checkBox( ResultPage.menu14_openHouse, parameter.checkKey("openHouse"));		
		element.checkBox( ResultPage.menu15_pendingListing, parameter.checkKey("pendingListing"));

		//Select properties between $300k and $700k
		String priceMin = parameter.get("priceMin");
		String priceMax = parameter.get("priceMax");
		if ((priceMin!=null) && (priceMax!=null)) {
			element.clickButton( ResultPage.menu2_price );
			WebElement webElement1 = element.getPresentElement( ResultPage.menu2_price_min );
			element.getWebElement( webElement1 , "li", priceMin).click();
			WebElement webElement2 = element.getPresentElement( ResultPage.menu2_price_max );
			element.getWebElement( webElement2 , "li", priceMax).click();
		}

		int bedroomNb = 0;
		if (parameter.checkKey("bed")) {
			bedroomNb = Value.convertStrInt( parameter.get("bed") );
			element.clickButton( ResultPage.menu3_beds );
			element.clickButton( ResultPage.getBed(bedroomNb) );
		}

		//Select only houses
		element.clickButton( ResultPage.menu4_homeType );
		element.checkBox( ResultPage.menu40_sfr, parameter.checkKey("sfr"));
		element.checkBox( ResultPage.menu41_apartment, parameter.checkKey("apartment"));
		element.checkBox( ResultPage.menu42_condo, parameter.checkKey("condo"));
		element.checkBox( ResultPage.menu43_townhouse, parameter.checkKey("townhouse"));
		element.checkBox( ResultPage.menu44_manufactured, parameter.checkKey("manufactured"));
		element.checkBox( ResultPage.menu45_lots, parameter.checkKey("lots"));
		logger.trace("Time to select the menu: "+ time.getSeconds(false));
		
		//Make sure the info of the properties are loaded
		//Thread.sleep(3000);
		element.getVisibleElement(ResultPage.resultCounter);
		logger.trace("Time for all the listings to appear on the right side: "+ time.getSeconds(false));


		//Gather all the real estate information into the variable named "properties" 
		getAllData(driver);
		logger.trace("Time to gather all the information: "+ time.getSeconds(false));

	
		//Check the type of property found
		if (!parameter.checkKey("saleListing"))
			assertEquals("Property for SALE were unexpected found", properties.countType(PropertyType.SALE), 0);
		if (!parameter.checkKey("pListing"))
			assertEquals("Property for LISTING were unexpected found", properties.countType(PropertyType.LISTING), 0);
		if (!parameter.checkKey("rent"))
			assertEquals("Property for RENT were unexpected found", properties.countType(PropertyType.RENT), 0);
		if (!parameter.checkKey("recentlySold"))
			assertEquals("Property for RECENTLY_SOLD were unexpected found", properties.countType(PropertyType.RECENTLY_SOLD), 0);

		//Check the number of bedrooms
		if (bedroomNb>0)
			for (int i=0; i<bedroomNb; i++)
				assertEquals("Property with "+ bedroomNb +" were unexpected found", properties.countBedroom(i), 0);
		
		//Statistic (just for fun)
		int nbProperty = properties.size();
		logger.info("The URL of the page contains \"Homes\":"+ element.checkURL("homes") );
		logger.info("The title of the page contains \"zillow\":"+ element.checkTitle("zillow") );
		logger.info(properties.countCity("Milpitas") +" out of "+ nbProperty +" properties are in Milpitas");
		logger.info(properties.countYear(1964) +" properties were built in 1964");
		logger.info(properties.countBedroom(4) +" properties have 4 bedrooms");
		logger.info(properties.countBathroom(2.0) +" properties have 2 bathrooms");

		//Finish test
		double t = time.getSeconds(true);
		logger.trace("checkSaleListing test DONE (Total time needed: "+ t +"s)");
		System.out.println("==> checkSaleListing test DONE in "+ t +"s");
	}

	// ########################################################################
	// GENERAL FUNCTIONS
	// ########################################################################

	private void logon(String logon_str, String password_str) {
		element.clickButton( LogonPage.logonButton );

		element.getVisibleElement(LogonPage.iFrameLogon);
		WebElement signIn = driver.findElement(LogonPage.iFrameLogon);
		driver.switchTo().frame(signIn);

		if ( element.isElementPresent(LogonPage.iFrameEmail) )
			element.inputForm(LogonPage.iFrameEmail, logon_str);
		if ( element.isElementPresent(LogonPage.iFramePassword) )
			element.inputForm(LogonPage.iFramePassword, password_str);

		element.clickButton(LogonPage.iFrameSubmit);
		//driver.switchTo().defaultContent();
	}

	private void selectMenu()
	{
		//Select only sale listings
		element.clickButton(ResultPage.menu1_listing);
		element.checkBox( ResultPage.menu10_saleListing, parameter.checkKey("saleListing"));
		element.checkBox( ResultPage.menu11_pListing, parameter.checkKey("pListing"));			//= potential listings
		element.checkBox( ResultPage.menu12_rent, parameter.checkKey("rent"));					
		element.checkBox( ResultPage.menu13_recentlySold, parameter.checkKey("recentlySold"));	
		element.checkBox( ResultPage.menu14_openHouse, parameter.checkKey("openHouse"));		
		element.checkBox( ResultPage.menu15_pendingListing, parameter.checkKey("pendingListing"));

		//Select properties between $300k and $700k
		String priceMin = parameter.get("priceMin");
		String priceMax = parameter.get("priceMax");
		if ((priceMin!=null) && (priceMax!=null)) {
			element.clickButton( ResultPage.menu2_price );
			WebElement webElement1 = element.getPresentElement( ResultPage.menu2_price_min );
			element.getWebElement( webElement1 , "li", priceMin).click();
			WebElement webElement2 = element.getPresentElement( ResultPage.menu2_price_max );
			element.getWebElement( webElement2 , "li", priceMax).click();
		}

		int bedroomNb = 0;
		if (parameter.checkKey("bed")) {
			bedroomNb = Value.convertStrInt( parameter.get("bed") );
			element.clickButton( ResultPage.menu3_beds );
			element.clickButton( ResultPage.getBed(bedroomNb) );
		}

		//Select only houses
		element.clickButton( ResultPage.menu4_homeType );
		element.checkBox( ResultPage.menu40_sfr, parameter.checkKey("sfr"));
		element.checkBox( ResultPage.menu41_apartment, parameter.checkKey("apartment"));
		element.checkBox( ResultPage.menu42_condo, parameter.checkKey("condo"));
		element.checkBox( ResultPage.menu43_townhouse, parameter.checkKey("townhouse"));
		element.checkBox( ResultPage.menu44_manufactured, parameter.checkKey("manufactured"));
		element.checkBox( ResultPage.menu45_lots, parameter.checkKey("lots"));
	}
	
	private void getAllData(WebDriver driver)
	{
		Property property;
		String type		= "";
		String address	= "";
		String data		= "";
		String lotsize	= "";
		String year		= "";
		String price	= "";
		String zEstimate= "";
		String doz		= "";
		
		//Gather all the data and save them into property
		List<WebElement> webElementList = driver.findElements( ResultPage.properties );
		for (WebElement webElement : webElementList) {
			//Get all data for each webElement
			/*
			String[] listingInfo	= webElement.getText().split("\n");
			for (String lineInfo : listingInfo)
				System.out.println("==>"+ lineInfo);
			*/
			address	= element.getData(webElement, ResultPage.propertyAddress, false);

			//Some listings are just ads, and are not considered
			if (!address.equals(""))
			{
				if (!"".equals( element.getData(webElement, ResultPage.propertyTypeSale, false) ))
					type = "sale";
				else if (!"".equals( element.getData(webElement, ResultPage.propertyTypeRent, false) ))
					type = "rent";
				else if (!"".equals( element.getData(webElement, ResultPage.propertyTypeListing, false) ))
					type = "MMM";
				else if (!"".equals( element.getData(webElement, ResultPage.propertyTypeRecentlySold, false) ))
					type = "RecentlySold";
				else
					type = "nbUnknown";
				
				data		= element.getData(webElement, ResultPage.propertyData, false);
				lotsize		= element.getData(webElement, ResultPage.propertyLotsize, false);
				year		= element.getData(webElement, ResultPage.propertyYear, false);
				price		= element.getData(webElement, ResultPage.propertyPrice, false);
				zEstimate	= element.getData(webElement, ResultPage.propertyZEstimate, false);
				doz			= element.getData(webElement, ResultPage.propertyDoz, false);

				property = new Property(type, address, data, lotsize, year, price, zEstimate, doz);
				properties.add(property);

				//Check the DB is all information are correct
				assertTrue("Property "+ property.getStreet() +" was not found in the DB", property.MySQLcheck());
			}
		}
	}
}