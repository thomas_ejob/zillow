package main;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import others.testMkyong;

@RunWith(Suite.class)
@SuiteClasses({ test.class, Zillow_4j2.class })
public class AllTests {
	//New > Java > JUnit > Test Suite
}
